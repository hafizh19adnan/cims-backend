from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView
from .models import Chemical
from .serializer import ChemicalSerializer
# Create your views here.


class AllChemical(ListAPIView):

    queryset = Chemical.objects.all()
    serializer_class = ChemicalSerializer

    def post(self, request, format=None):
        serializer = ChemicalSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChemicalView(APIView):

    def get(self, request, pk, format=None):
        try:
            chemical = Chemical.objects.get(pk=pk)
            serializer = ChemicalSerializer(chemical)
            return Response(serializer.data)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, pk, format=None):
        chemical = Chemical.objects.get(pk=pk)
        chemical.delete()
        return Response(status=status.HTTP_200_OK)