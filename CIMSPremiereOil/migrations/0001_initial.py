# Generated by Django 3.1.1 on 2020-09-14 15:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Chemical',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chemical_name', models.CharField(max_length=200)),
                ('ocns', models.CharField(max_length=20)),
            ],
        ),
    ]
